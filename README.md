# Next.js example

## How to use

Download the example [or clone the repo](https://github.com/mui-org/material-ui):

Install it and run:

```sh
npm install
npm run dev
```

## The idea behind the example

[Next.js](https://github.com/zeit/next.js) is a framework for server-rendered React apps.
