import {
  Box,
  Button,
  CircularProgress,
  Grid,
  TextField,
  Typography,
  InputAdornment,
} from "@material-ui/core";
import React from "react";
import Page from "../components/Page";
import NumberInput from "../components/Input/NumberInput";
import { useFormik } from "formik";
import * as yup from "yup";
import JSONPretty from "react-json-pretty";
import Axios from "axios";
import CustomSnackbar from "../components/CustomSnackbar";

const validationSchema = yup.object({
  query: yup.string("Required Field").required("Required Field"),
});

export default function Index() {
  const [loading, setLoading] = React.useState(false);
  const [disabled, setDisabled] = React.useState(false);
  const [useSnackbar, setUseSnackbar] = React.useState({ open: false });
  const [data, setData] = React.useState(null);

  const formik = useFormik({
    validationSchema,
    enableReinitialize: true,
    initialValues: {
      query: "",
    },
    onSubmit: async (values, { setSubmitting, setFieldValue, resetForm }) => {
      const { query } = values;
      setLoading(true);
      setDisabled(true);
      const { data } = await Axios.get(
        `${process.env.SERVER_URL}/transform?decimal=R$${query}`
      );
      console.log(data);
      if (data.status == 400)
        setUseSnackbar({
          open: true,
          message: data.message,
          severity: "error",
        });
      else if (data.status == 200)
        setUseSnackbar({
          open: true,
          message: data.message,
          severity: "success",
          duration: 10000
        });
        
      setLoading(false);
    },
  });

  return (
    <Page>
      <Grid container>
        <Grid item xs={12}>
          <Box style={{ margin: 10, marginBottom: 40, textAlign: "justify" }}>
            <Typography>
              Welcome to this app! You will see how many bills will be needed
              for your value to be delivered. You have some manners about how
              to:
            </Typography>
            <br />
            <Typography>&#9679; 160,00</Typography>
            <Typography>&#9679; 12540</Typography>
          </Box>
          <NumberInput
            fieldName={"query"}
            formik={formik}
            fullWidth
            label="Input amount of money you want *"
            placeholder="Money you want *"
            color="secondary"
            variant="outlined"
            CustomInputProps={{
              endAdornment: (
                <InputAdornment position="end" style={{ marginRight: 10 }}>
                  R$
                </InputAdornment>
              ),
            }}
          />
          <Typography style={{ fontSize: 12, color: "gray", marginTop: 5 }}>
            * REQUIRED FIELD
          </Typography>
          <Box style={{ margin: 10, textAlign: "center" }}>
            <Button
              variant="contained"
              color="primary"
              style={{ color: "white" }}
              disabled={!(formik.isValid && formik.dirty) || loading || disabled}
              onClick={formik.handleSubmit}
            >
              {loading ? <CircularProgress size={20} /> : "Get Info"}
            </Button>
          </Box>
        </Grid>
        {/* <Grid item xs={12} md={6}>
          <Box style={{ paddingLeft: 50 }}>
            <Typography style={{ marginBottom: 20, marginTop: 10 }}>
              Result
            </Typography>
            <JSONPretty id="json-pretty" data={data}></JSONPretty>
          </Box>
        </Grid> */}
      </Grid>
      {useSnackbar.open && (
        <CustomSnackbar
          setUseSnackbar={setUseSnackbar}
          setDisabled={setDisabled}
          isOpen={useSnackbar.open}
          message={useSnackbar.message}
          duration={useSnackbar.duration}
          severity={useSnackbar.severity}
        />
      )}
    </Page>
  );
}
