import React from 'react';
import NumberFormat from 'react-number-format';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
	textoErro: {
    fontWeight: 400,
    color: "#d72027",
  },
}));


function NumberFormatCustom(props) {
  const { inputRef, onChange, thousandSeparator, format,...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={onChange}
			format={format}
			isNumericString
			thousandSeparator={thousandSeparator}
    />
  );
}


export default function NumberInput(props) {
  const classes = useStyles();
	const {formik, fieldName, CustomInputProps, noError, helperStyle, thousandSeparator, format} = props

  const handleChange = (e) => {
		formik.setFieldValue(fieldName, e.value);
  };

  return (
		<TextField
			className={classes.textField}
			value={formik.values[fieldName]}
			onChange={handleChange}
			thousandSeparator={thousandSeparator}
			onBlur={formik.handleBlur}
			error={formik.touched[fieldName] && formik.errors[fieldName] && !noError}
			helperText={formik.touched[fieldName] && formik.errors[fieldName] && !noError ? formik.errors[fieldName] : ''}
			name={fieldName}
			inputProps={{
        className: classes.root,
				thousandSeparator,
				format
      }}
			InputProps={{
				...CustomInputProps, 
				inputComponent: NumberFormatCustom,
				classes: {
					root: classes.input
				},
			}}
			FormHelperTextProps={helperStyle? { style: helperStyle } : { className: classes.textoErro} }
			{...props}
		/>
  );
}
