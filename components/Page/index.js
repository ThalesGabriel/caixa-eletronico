import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import ProTip from "./components/ProTip";
import Copyright from "./components/Copyright";
import Navigation from "./components/Navigation";
import { Container } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  containerPrincipal: {
    display: "flex",
		flexDirection: 'column',
		height: '100vh'
  },
}));

export default function Page({ children }) {
  const classes = useStyles();

  return (
    <Container maxWidth="lg" className={classes.containerPrincipal}>
      <Navigation/>
      <Box my={12}>{children}</Box>
      <Box style={{marginTop: 'auto'}}>
        <ProTip />
        <Copyright />
      </Box>
    </Container>
  );
}