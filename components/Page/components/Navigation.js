import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import GitHubIcon from '@material-ui/icons/GitHub';
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  menuButton: {
    textAlign: 'flex-end'
  },
  title: {
    flexGrow: 1,
  },
}));

export default function Navigation() {
  const classes = useStyles();
  const router = useRouter();
  return (
    <div className={classes.root}>
      <AppBar position="absolute">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Welcome to caixa eletrônico app
          </Typography>
            <a style={{color: 'white'}} target="_blank" href="https://bitbucket.org/ThalesGabriel/caixa-eletronico/src/main/" rel="noopener noreferrer">
              <IconButton className={classes.menuButton} color="inherit" aria-label="menu">
                <GitHubIcon />
              </IconButton>
            </a>
        </Toolbar>
      </AppBar>
    </div>
  );
}